<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบ SGD Line Warning - เข้าสู่ระบบ</title>
  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>

  <?php include('../../inc/css-header.php'); session_destroy(); ?>
  <link rel="stylesheet" href="css/login.css">

</head>
<body class="hold-transition login-page" onload="showProcessbar();">
<div class="login-box">
  <div class="login-logo">
        <b style=" color:#093"><i class="fa fa-bell-o" aria-hidden="true"></i>SGD</b>
        <p style=" color:#fff;font-size:40px;">Line Warning<p>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">

    <div align="center" style="margin-bottom:10px;">
      <img src="../../image/logo.png" style="height:120px;">
    </div>
    <p class="login-box-msg">ล็อกอินเข้าสู่ระบบ</p>
      <form id="formLogin" autocomplete="off" novalidate >
      <!-- <form method="post" action="ajax/AEDLogin.php" data-smk-icon="glyphicon-remove-sign" novalidate> -->
        <div id="show-form"></div>
      </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?php include('../../inc/js-footer.php'); ?>
<script src="js/login.js"></script>
</body>
</html>
