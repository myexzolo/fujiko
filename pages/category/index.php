<!DOCTYPE html>
  <?php
  $pageName     = "Category";
  $pageCode     = "";
  ?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ระบบ Back Office - <?= $pageName ?></title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/category.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              <?= $pageName ?>
              <small><?=$pageCode ?></small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-home"></i> หน้าหลัก</a></li>
              <li class="active"><?= $pageName ?></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php"); ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12">
                <div class="box box-warning">
                  <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <?php if($_SESSION['ROLE_USER']['is_insert'])
                    {
                    ?>
                      <a class="btn btn-social btn-success btnAdd pull-right" onclick="showForm('ADD')">
                        <i class="fa fa-plus"></i> Category
                      </a>
                    <?php
                    }
                    ?>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body" style="min-height: 200px;">
                    <div id="showTable"></div>
                  </div>
                  <div class="box-footer">
                    <button type="button" onclick="formSeqImage()" style="width:190px;height:40px;font-size:22px;" class="btn btn-primary btn-flat pull-right">
                      <i class="fa fa-th-list"></i>&nbsp;
                      จัดเรียง Category
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.row -->
            <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">จัดการ Category</h4>
                  </div>
                    <form id="formAdd" novalidate enctype="multipart/form-data">
                    <!-- <form novalidate action="ajax/AEDModule.php" method="post" enctype="multipart/form-data"> -->
                      <div id="show-form"></div>
                    </form>
                </div>
              </div>
            </div>
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/category.js"></script>
    </body>
  </html>
