<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
 .img-item {
   height: 200px;
   width: auto;
 }
 .btn-remove {
   color: #3b3b3b;
   background: rgba(232, 235, 233, 0.7);
   border-width: 0px !important;
 }

 /* .my-item {
   background: rgba(232, 235, 233, 1);
 } */

 .btn-remove:hover {
   color: #5a635d;
   background: rgba(232, 235, 233, 0.9);
 }
</style>
<form id="formSeqImage" novalidate enctype="multipart/form-data">
<div class="my-container">
<?php
$sqls   = "SELECT * FROM t_category ORDER BY cast(category_seq as unsigned)";
$querys = DbQuery($sqls,null);
$json   = json_decode($querys, true);
$counts = $json['dataCount'];
$rows   = $json['data'];

if($counts > 0){
foreach ($rows as $key => $value) {
?>
  <div class="my-item">
    <div style="position: absolute;top :30px;width:100%; font-size:26px">
      <div style="text-align:center;width:100%;background: rgba(232, 235, 233, 0.5);margin-bottom:5px;"><?=$value['category_name_th'];?></div>
      <div style="text-align:center;background: rgba(232, 235, 233, 0.5);"><?=$value['category_name_en'];?></div>
    </div>
    <img class="img-item" src="upload/<?=$value['category_path'];?>">
    <div style="margin-top:-26px;">
      <button type="button"
       style="height:24px;width:49%; font-size:16px;line-height: 1px;"
       class="btn btn-flat btn-remove" onclick="editImage('<?=$value['category_id'];?>')" >
        <i class="fa fa-edit"></i>
      </button>
    <button type="button"
     style="height:24px;width:49%; font-size:16px;line-height: 1px;"
     class="btn btn-flat btn-remove" onclick="deleteImage('<?=$value['category_id'];?>')" >
      <i class="fa fa-trash-o"></i>
    </button>
    </div>
    <input type="hidden" name="slideID[]" value="<?=$value['category_id'];?>">
  </div>
<?php } } ?>
</div>
</form>
<script>
$('.my-container').sortablePhotos({
  selector: '> .my-item',
  sortable: true,
  padding: 5
});
</script>
