<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$access_token = getAccessToken();
// User ID
$userId       = isset($_POST['userId'])?$_POST['userId']:"";
$name         = isset($_POST['name'])?$_POST['name']:"";
$lname        = isset($_POST['lname'])?$_POST['lname']:"";
$email        = isset($_POST['email'])?$_POST['email']:"";
$mobile       = isset($_POST['mobile'])?$_POST['mobile']:"";
$code         = isset($_POST['code'])?$_POST['code']:"";


//print_r($_POST);

// --ADD EDIT DELETE Module-- //
if($userId != "")
{
  if($code != "")
  {
    $sql   = "SELECT * FROM t_code_nvr WHERE code = '$code' and is_active <> 'D' ";

    $querys     = DbQuery($sql,null);
    $json       = json_decode($querys, true);
    $errorInfo  = $json['errorInfo'];
    $dataCount  = $json['dataCount'];
    $rows       = $json['data'];
    if($dataCount > 0)
    {
      $sql   = "SELECT * FROM t_member WHERE code = '$code' and user_id = '$userId' and is_active <> 'D' ";

      $querym     = DbQuery($sql,null);
      $jsonm       = json_decode($querym, true);
      $dataCountm  = $jsonm['dataCount'];
      $rowm         = $jsonm['data'];

      if($dataCountm > 0){
        $message = "คุณ".$rowm[0]['name']." ".$rowm[0]['lname']." สมัครการแจ้งเตือน ซีเรียลนัมเบอร์ : ".$rowm[0]['code']." แล้ว !!";
        header('Content-Type: application/json');
        exit(json_encode(array('status' => 'danger','message' => $message)));
      }else{
        $sqls   = "INSERT INTO t_member
                  (name,lname,email,mobile,code,user_id,is_active)
                  VALUES
                  ('$name','$lname','$email','$mobile','$code','$userId','Y')";

        $query      = DbQuery($sqls,null);
        $row        = json_decode($query, true);
        $errorInfo  = $row['errorInfo'];

        if(intval($row['errorInfo'][0]) == 0){
          $json = [
            "type"=> "text",
            'text' => "ลงทะเบียนแจ้งเตือน สำเร็จ"
          ];

          $post = json_encode(array(
              'to' => array($userId),
              'messages' => array($json),
          ));
          //URL ของบริการ Replies สำหรับการตอบกลับด้วยข้อความอัตโนมัติ
          $url = 'https://api.line.me/v2/bot/message/multicast';
          $headers = array('Content-Type: application/json', 'Authorization: Bearer '.$access_token);
          $ch = curl_init($url);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          $result = curl_exec($ch);

          //echo $sqlAcc;
          header('Content-Type: application/json');
          exit(json_encode(array('status' => 'success','message' => 'Success')));
        }else{
          header('Content-Type: application/json');
          exit(json_encode(array('status' => 'danger','message' => 'Fail')));
        }
      }
    }else{
      header('Content-Type: application/json');
      exit(json_encode(array('status' => 'danger','message' => 'ไม่พบซีเรียลนัมเบอร์ เครื่องบันทึก')));
    }
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'ไม่พบซีเรียลนัมเบอร์ เครื่องบันทึก')));
  }
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'ไม่พบ UserId Line')));
}
?>
