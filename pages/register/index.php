<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบ SGD Line Warning - ลงทะเบียนแจ้งเตือน</title>
  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>

  <?php
    include('../../inc/css-header.php');
    include('../../inc/function/mainFunc.php');
    session_destroy();
    $userId = $_GET['userId'];

    $client_id = getClientID();
    $callback_url = getCallbackUrl();

  ?>
  <link rel="stylesheet" href="css/reg.css">

</head>
<body class="hold-transition login-page" onload="showProcessbar();">
<div class="login-box">
  <div class="login-logo">
        <b style=" color:#093"><i class="fa fa-bell-o" aria-hidden="true"></i>SGD</b>
        <p style=" color:#fff;font-size:40px;">Line Warning<p>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <div align="center" style="margin-bottom:10px;">
      <img src="../../image/logo.png" style="height:120px;">
    </div>
    <p class="login-box-msg">ลงทะเบียนแจ้งเตือน</p>

    <!-- <form novalidate action="ajax/AED.php" method="post"> -->
      <form id="formRegister" autocomplete="off" novalidate >
        <div class="form-group has-feedback">
          <input type="hidden" value="<?=$client_id?>" id="client_id">
          <input type="hidden" value="<?=$callback_url?>" id="callback_url">
          <input type="hidden" value="<?=$userId?>" name="userId">
          <input type="text" class="form-control" name="name" placeholder="ชื่อ" required>
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="text" class="form-control" name="lname" placeholder="นามสกุล" required>
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="email" class="form-control"  name="email" placeholder="Email" required>
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="tel" class="form-control"  name="mobile" placeholder="เบอร์โทรติดต่อ" required>
          <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="text" class="form-control"  name="code" placeholder="ซีเรียลนัมเบอร์ เครื่องบันทึก" required>
          <span class="glyphicon glyphicon-tags form-control-feedback"></span>
        </div>
        <button type="submit" id="sendForm" class="btn btn-primary btn-block btn-flat" >สมัคร</button>
      </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?php include('../../inc/js-footer.php'); ?>
<script src="js/reg.js"></script>
</body>
</html>
