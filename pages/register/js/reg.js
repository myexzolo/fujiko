$('#formRegister').on('submit', function(event) {
  event.preventDefault();
  if ($('#formRegister').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAdd').smkClear();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        if(data.status == "success"){
          var URL = 'https://notify-bot.line.me/oauth/authorize?';
          var client_id = $('#client_id').val();
          var callback_url = $('#callback_url').val();
          URL += 'response_type=code';
          URL += '&client_id='+ client_id;
          URL += '&redirect_uri='+callback_url;//ถ้า login แล้ว เลือกกลุ่มหรือตัวเอง ให้กลับมาหน้านี้
          URL += '&scope=notify';
          URL += '&state='+data.id;//กำหนด  user หรือ อะไรก็ได้ที่สามารถบอกถึงว่าเป็น user ในระบบ
          window.location.href = URL;
        }
      }, 1000);
    });
  }
});
