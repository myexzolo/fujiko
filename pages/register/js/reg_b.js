$('#formRegister').on('submit', function(event) {
  event.preventDefault();
  if ($('#formRegister').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAdd').smkClear();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        if(data.status == "success"){
          gotoPage("../welcome");
        }
      }, 1000);
    });
  }
});
