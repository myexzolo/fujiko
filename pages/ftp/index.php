<!DOCTYPE html>
  <?php
  $pageName     = "จัดการ FTP";
  $pageCode     = "";
  ?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ระบบ Back Office - <?= $pageName ?></title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/ftp.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              <?= $pageName ?>
              <small><?=$pageCode ?></small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-home"></i> หน้าหลัก</a></li>
              <li class="active"><?= $pageName ?></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php"); ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12">
                <div class="box box-primary">
                  <div class="box-header with-border">

                    <?php if($_SESSION['ROLE_USER']['is_insert'])
                    {
                    ?>
                      <h3 class="box-title">
                          <?php //print_r($_SESSION['ROLE_USER']); ?>
                      </h3>
                      <div class="box-tools">
                        <!-- <div class="pull-right">
                          <a class="btn btn-block btn-social btn-success btnAdd" onclick="showForm('ADD')">
                            <i class="fa fa-plus"></i> Code
                          </a>
                        </div> -->
                        <div class="pull-right">
                          <a class="btn btn-block btn-social btn-primary btnAdd" onclick="showForm()">
                            <i class="fa fa-file-excel-o"></i> นำเข้า TXT
                          </a>
                        </div>
                      </div>
                    <?php
                    }
                    ?>

                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div id="showTable"></div>
                  </div>
                </div>


                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Management Ftp</h4>
                      </div>
                      <form id="formAdd" novalidate enctype="multipart/form-data">
                      <!-- <form novalidate action="ajax/manageUpload.php" method="post" enctype="multipart/form-data"> -->
                        <div id="show-form"></div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/ftp.js"></script>
    </body>
  </html>
