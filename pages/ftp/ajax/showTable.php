<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px">ลำดับ</th>
      <th>User FTP</th>
      <th>Password</th>
      <th>สถานะ</th>
      <th style="width:70px;">Del</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $sql   = "SELECT * FROM t_ftp WHERE is_active <> 'D' order by id desc";

      $querys     = DbQuery($sql,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      if($dataCount > 0){
      foreach ($rows as $key => $value) {
        $is_active = $value['is_active'];
        $id    = $value['id'];
        $user   = $value['user'];
        $is_active = $value['is_active'];
        $disable = "";
        $onclick = "";
        if($is_active == 'S')
        {
            $txt_active = "ว่าง";
            $onclick = "onclick=\"delModule('$id','$user')\"";
        }else if ($is_active == 'Y'){
            $txt_active = "ใช้งาน";
        }else if ($is_active == 'N'){
            $txt_active = "ไม่ใช้งาน";
        }
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="left"><?=$user;?></td>
      <td align="left"><?=$value['pw'];?></td>
      <td><?=$txt_active;?></td>
      <td>
        <a class="btn_point text-red <?= $disable ?>" <?= $onclick ?>><i class="fa fa-trash-o" ></i></a>
      </td>
    </tr>
  <?php } } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
