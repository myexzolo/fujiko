
          <!-- /.box-header -->
<div class="box-body">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="form-group col-md-12">
          <label class="col-sm-4 control-label">นำเข้า ไฟล์ TXT</label>
          <div class="col-sm-8">
            <input type="file" name="filepath" accept=".txt,.csv" required class="custom-file-input form-control">
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
<button type="submit" style="width:100px;" class="btn btn-success btn-flat">นำเข้า</button>
</div>
