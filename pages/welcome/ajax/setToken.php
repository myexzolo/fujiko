<?php
  include('../../../inc/function/connect.php');
  include('../../../inc/function/mainFunc.php');
  header("Content-type:text/html; charset=UTF-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);

  $code   = $_POST['code'];
  $state  = $_POST['state'];

  // $code   = "acXOSVCMqbbYTK3WxchYo6";
  // $state  = "33";

  $client_id = getClientID();
  $client_secret = getClientSecret();

  $api_url = 'https://notify-bot.line.me/oauth/token';
  $callback_url = getCallbackUrl();


  $fields = [
      'grant_type' => 'authorization_code',
      'code' => $code,
      'redirect_uri' => $callback_url,
      'client_id' => $client_id,
      'client_secret' => $client_secret
  ];

  try {
      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, $api_url);
      curl_setopt($ch, CURLOPT_POST, count($fields));
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

      $res = curl_exec($ch);
      curl_close($ch);

      if ($res == false){
        throw new Exception(curl_error($ch), curl_errno($ch));
        header('Content-Type: application/json');
        exit(json_encode(array('status' => '400')));
      }


      $json = json_decode($res, true);
      // print_r($json);
      if($json['status'] == 200)
      {
        $line_token = $json['access_token'];
        $sql  = "UPDATE t_member SET line_token = '$line_token' where id = $state";
        $query      = DbQuery($sql,null);
        $row        = json_decode($query, true);

        $sqls  = "SELECT m.code,f.user,f.pw FROM t_member m, t_code_nvr n,t_ftp f
                 where m.id = $state and m.code = n.code and n.id_ftp = f.id";
       $querys = DbQuery($sqls,null);
       $json   = json_decode($querys, true);
       $dataCount  = $json['dataCount'];
       $rows       = $json['data'];

       if($dataCount > 0)
       {
         $message  = "";
         $message  .= "\r\nระบบแจ้งเตือน";
         $message  .= "\r\n--------------------------";
         $message  .= "\r\nซีเรียลนัมเบอร์ : ".$rows[0]['code'];
         $message  .= "\r\nUserName : ".$rows[0]['user'];
         $message  .= "\r\nPassword : ".$rows[0]['pw'];
         $message  .= "\r\n--------------------------";
         $message  .= "\r\nCCTV FUJIKO & KENPRO";

         sendLineMessages($line_token,$message);
       }

        //print_r($row);
        header('Content-Type: application/json');
        exit(json_encode(array('status' => '200')));
      }else{
        $sql  = "DELETE FROM t_member where id = $state";
        DbQuery($sql,null);

        header('Content-Type: application/json');
        exit(json_encode(array('status' => '400')));
      }
      //var_dump($json);
  } catch(Exception $e) {
      throw new Exception($e->getMessage());
      header('Content-Type: application/json');
      exit(json_encode(array('status' => '400')));
      //var_dump($e);
  }

  function sendLineMessages($token,$message)
  {
    $fields = [
                  'message' => $message
              ];

    $headers = [
                  'Authorization: Bearer ' . $token
              ];
    //URL ของบริการ Replies สำหรับการตอบกลับด้วยข้อความอัตโนมัติ
    // $url = 'https://api.line.me/v2/bot/message/multicast';
    // $url = 'https://api.line.me/v2/bot/message/push';
    $url = 'https://notify-api.line.me/api/notify';
    //$headers = array('Content-Type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$token);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


    $result = curl_exec($ch);
    curl_close($ch);
    //$arrRes = json_decode($result);
    //echo $result."\r\n";
  }
?>
