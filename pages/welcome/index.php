<!DOCTYPE html>
<?php
  $code   = $_GET['code'];
  $state  = $_GET['state'];
?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบ SGD Line Warning - Welcome</title>
  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>

  <?php include('../../inc/css-header.php'); session_destroy(); ?>
  <link rel="stylesheet" href="css/welcome.css">

</head>
<body class="hold-transition login-page" onload="showProcessbar();">
<div class="login-box">
  <div class="login-logo">
        <b style=" color:#093"><i class="fa fa-bell-o" aria-hidden="true"></i>SGD</b>
        <p style=" color:#fff;font-size:40px;">Line Warning<p>
  </div>
  <div align="center" style="margin-top:calc(100% - 7em);">
    <input type="hidden" value="<?=$code?>" id="code">
    <input type="hidden" value="<?=$state?>" id="state">
    <p style=" color:#fff;font-size:30px;" id="txtStatus"><p>
  </div>

</div>
<!-- /.login-box -->

<?php include('../../inc/js-footer.php'); ?>
<script src="js/welcome.js"></script>
</body>
</html>
