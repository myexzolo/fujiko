<?php
require_once '../../../Classes/PHPExcel.php';

include '../../../Classes/PHPExcel/IOFactory.php';
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');


$target_file    = $_FILES["filepath"]["name"];
$inputFileName  = $target_file;

if (file_exists($target_file)) {
  unlink($target_file);
}

$success = 0;
$fail    = 0;
$total   = 0;

$type_test = 1;

if (move_uploaded_file($_FILES["filepath"]["tmp_name"], $target_file)) {
   try
   {
       chmod($inputFileName, 0775);
       $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
       $objReader = PHPExcel_IOFactory::createReader($inputFileType);
       $objReader->setReadDataOnly(true);
       $objPHPExcel = $objReader->load($inputFileName);

       $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
       $highestRow = $objWorksheet->getHighestRow();
       $highestColumn = $objWorksheet->getHighestColumn();

       $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
       $headingsArray = $headingsArray[1];

       $text = $headingsArray['A'];

       if(trim($text) != "")
       {
         $headingsArray = array("A" => "model",
                                "B" => "serial"
                              );
          $r = -1;

          $namedDataArray = array();
          for ($row = 2; $row <= $highestRow; ++$row) {
              $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
              if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
                  ++$r;
                  foreach($headingsArray as $columnKey => $columnHeading) {
                      $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
                  }
              }
          }
          $i = 0;
          $arrData = array();
          $errNvr  = array();
          if(count($namedDataArray) > 0)
          {
            foreach ($namedDataArray as $result) {
               try
               {
                     $total++;
                     $model    = trim($result["model"]);
                     $serial   = trim($result["serial"]);

                     $sql = "SELECT * FROM t_code_nvr WHERE code = '$serial'";
                     $query  = DbQuery($sql,null);
                     $json   = json_decode($query,true);
                     $count  = $json['dataCount'];
                     if($count == 0)
                     {
                       $sql   = "SELECT * FROM t_ftp WHERE is_active = 'S' order by id asc";

                       $querys     = DbQuery($sql,null);
                       $json       = json_decode($querys, true);
                       $errorInfo  = $json['errorInfo'];
                       $dataCount  = $json['dataCount'];
                       $rows       = $json['data'];

                       if($dataCount > 0)
                       {
                         $id_ftp = $rows[0]['id'];
                         $user   = $rows[0]['user'];

                         $sql   = "INSERT INTO t_code_nvr
                                   (code,name,detail,create_date,is_active,id_ftp)
                                   VALUES
                                   ('$serial','$model','',NOW(),'Y','$id_ftp');";
                         $sql   .= "UPDATE t_ftp SET is_active = 'Y' where id = '$id_ftp';";

                         $query      = DbQuery($sql,null);
                         $row        = json_decode($query, true);
                         $errorInfo  = $row['errorInfo'];

                         if(intval($row['errorInfo'][0]) == 0){
                           $success++;
                         }else{
                           $fail++;
                           array_push($errNvr,$serial);
                         }
                       }else{
                         $fail++;
                         array_push($errNvr,$serial);
                       }
                     }else{
                       $fail++;
                       array_push($errNvr,$serial);
                     }
               }catch (Exception $ex) {
                 $fail++;
                 array_push($errNvr,$serial);
               }
            }

            if($total == $success)
            {
              $status_upload = "S";
            }else{
              $status_upload = "F";
            }

            $errNvr = implode(",",$errNvr);

            $str = "INSERT INTO t_history_upload
                    (file_name,status_upload,total_record,success_record,fail_record,fail_list,type_file)
                    VALUES
                    ('$inputFileName','$status_upload','$total','$success','$fail','$errNvr','nvr')";
             //echo $str;
             DbQuery($str,null);

            header('Content-Type: application/json');
            exit(json_encode(array('status' => 'success', 'message' => 'Success' , 'data' => $arrData, 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
          }else{
            header('Content-Type: application/json');
            exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบข้อมูล', 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
          }
      }
   } catch (Exception $e) {
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง', 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
   }
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง', 'success'=>$success, 'fail'=>$fail, 'total'=>$total )));
  //echo "ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง";
}


?>
