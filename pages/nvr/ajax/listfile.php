<?php
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$code   = isset($_POST['code'])?$_POST['code']:"";

$rootpath = "../../../../ftp_access_motion/$code";


 function format_bytes(int $size){
    $base = log($size, 1024);
    $suffixes = array('', 'KB', 'MB', 'GB', 'TB');
    return round(pow(1024, $base-floor($base)), 2).''.$suffixes[floor($base)];
}
?>
<div class="modal-body">
<input type="hidden" name="code" value="<?=$code?>">
<table class="table table-bordered table-striped" id="tableDisplay2">
  <thead>
    <tr class="text-center">
      <th><input id="checkApp" type="checkbox" onclick="checkAll(this)" checked></th>
      <th>Name</th>
      <th>Date</th>
      <th>Type</th>
      <th>Size</th>
    </tr>
  </thead>
  <tbody>
<?php
if (file_exists("$rootpath"))
{
  $files = glob($rootpath."/*"); // get all file names
  foreach($files as $file)
  { // iterate files
    if(is_file($file))
    {
      //echo $file;
      $fileInfo = pathinfo($file);
  ?>
  <tr class="text-center">
    <td><input class="checkId" onchange="chkList()" type="checkbox" value="<?=$file ?>" name="pathFile[]"></td>
    <td align="left"><?= $fileInfo['basename'] ?></td>
    <td><?= date("d-m-Y H:i:s", filemtime($file)); ?></td>
    <td><?= $fileInfo['extension'] ?></td>
    <td align="right"><?= format_bytes(filesize($file)) ?></td>
  </tr>
<?php
    }
  }
}
?>
</tbody>
</table>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">ลบ</button>
</div>
<script>
$(function () {
  chkList();
  $('#tableDisplay2').DataTable({
    'paging'      : false,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : false,
    'info'        : true,
    'autoWidth'   : false
  })
})
</script>
