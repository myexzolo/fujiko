<?php

$code       = isset($_POST['code'])?$_POST['code']:"";
$pathFile   = isset($_POST['pathFile'])?$_POST['pathFile']:"";

if($pathFile != null && $pathFile != "" && count($pathFile) > 0)
{
    $count = count($pathFile);

    for($i=0; $i < $count;$i++)
    {
      $file = $pathFile[$i];
      if(is_file($file)){
        unlink($file);
      }
    }
}

header('Content-Type: application/json');
exit(json_encode(array('status' => 'success','message' => 'Success', 'code' => $code)));
?>
