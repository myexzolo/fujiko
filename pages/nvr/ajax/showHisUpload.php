<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$type_file    = !empty($_POST['type_file'])?"{$_POST['type_file']}":"";

?>

<div class="box-body">
  <table class="table table-bordered table-striped table-hover" id="tableUpload">
    <thead>
        <tr>
          <th>ลำดับ</th>
          <th>ชื่อไฟล์</th>
          <th>วันที่นำเข้า</th>
          <th>สถานะ</th>
          <th>ทั้งหมด</th>
          <th>สำเร็จ</th>
          <th>ล้มเหลว</th>
          <th>เลขที่ล้มเหลว</th>
        </tr>
    </thead>
    <tbody>
      <?php

        $sql  = "SELECT * FROM t_history_upload WHERE type_file = '$type_file' order by history_upload_id desc";

        $query     = DbQuery($sql,null);
        $json       = json_decode($query, true);
        $errorInfo  = $json['errorInfo'];
        $dataCount  = $json['dataCount'];
        $rows       = $json['data'];

      if($dataCount > 0)
      {
        foreach ($rows as $key => $value) {
          $fail_list = $value['fail_list'];
          if($fail_list != ""){
             $fail_list = explode(",",$fail_list);
             $fail_list = implode("<br>",$fail_list);
          }
        ?>
        <tr class="text-center">
          <td><?=$key+1;?></td>
          <td><?=$value['file_name'];?></td>
          <td><?=DateTimeThai($value['date_upload']);?></td>
          <td><?=$value['status_upload']=='F'?"ผิดพลาด":"สมบูรณ์";?></td>
          <td><?=$value['total_record'];?></td>
          <td><?=$value['success_record'];?></td>
          <td><?=$value['fail_record'];?></td>
          <td><?= $fail_list ?></td>
        </tr>
    <?php }} ?>
    </tbody>
  </table>
</div>
<script>
  $(function () {
    $('#tableUpload').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : false
    })
  })
</script>
