
$(function () {
    showTable();
})


function showTable(){
  $.get( "ajax/showTable.php")
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function showListfile(code){
  $.post("ajax/listfile.php",{code:code})
    .done(function( data ) {
      $('#myModalFile').modal({backdrop:'static'});
      $('#show-form-file').html(data);
  });
}

function showForm(action="",id=""){
  $.post("ajax/form.php",{action:action,id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}

function delModule(id,code){
  $.smkConfirm({
    text:'ยืนยันการลบข้อมูล Code : '+ code +' ?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AED.php",{action:'DEL',id:id,code:code})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAdd').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAdd').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});

$('#formDataUpload').on('submit', function(event) {
  event.preventDefault();
  if ($('#formDataUpload').smkValidate()) {
    $.ajax({
        url: 'ajax/manageUpload.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formDataUpload').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal2').modal('toggle');
      }, 1000);
    });
  }
});



$('#formFile').on('submit', function(event) {
  event.preventDefault();
  if ($('#formFile').smkValidate()) {
    $.ajax({
        url: 'ajax/delfile.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      showListfile(data.code);
    });
  }
});


function checkAll(obj) {
  if(obj.checked){
    $('.checkId').each(function(){
        $(this).prop('checked', true);
    });
  }else{
    $('.checkId').prop('checked', false);
  }
}

function chkList(){
  $( ".checkId" ).each(function( index ) {
      if(!this.checked){
        $('#checkApp').prop('checked', false);
        return false;
      }
  });
}

function upload(){
  $('#myModal2').modal({backdrop:'static'});
  $.post("ajax/formUpload.php",{})
  .done(function( data ) {
      $("#formShow2").html(data);
  });
}

function showTableHistory(project_code){
  $.post("ajax/showHisUpload.php",{type_file:'nvr'})
  .done(function( data ) {
      $("#show-history").html(data);
  });
}
