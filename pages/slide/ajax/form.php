<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = @$_POST['value'];

?>
<input type="hidden" id="action" name="action" value="<?= $action ?>">
<div class="modal-body">
  <div class="row">
    <div style="margin-top:0px;padding:20px;min-height: 52vh;">
      <!-- <div id="myDropzone" class="dropzone"></div> -->
      <form action="ajax/AEDModule.php" class="dropzone" id="myDropzone">
        <input type="hidden" id="id" name="id" value="">
      </form>
    </div>
  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
<?php if($action != "SHOW"){ ?>
<button type="button" id="submit-all" style="width:100px;" class="btn btn-primary btn-flat">เพิ่ม</button>
<?php } ?>
</div>


<script type="text/javascript">

// Dropzone.autoDiscover = false;
var id = $('#id').val();
var myDropzone = new Dropzone("#myDropzone",
  {
    autoProcessQueue: false,
    addRemoveLinks:true,
    thumbnailWidth: 100,
    thumbnailHeight: 100,
    parallelUploads: 100,
    acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
    dictDefaultMessage: "ลากและวางไฟล์รูปภาพ",
    init: function(){
       var submitButton = document.querySelector('#submit-all');
       var myDropzone = this;
       var num = 0;
       submitButton.addEventListener("click", function(){
         if(myDropzone.getQueuedFiles().length == 0){
           $.smkProgressBar({
             element:'body',
             status:'start',
             bgColor: '#000',
             barColor: '#fff',
             content: 'Loading...'
           });
           setTimeout(function(){
             $.smkProgressBar({status:'end'});
             showTable();
             showSlidebar();
             $.smkAlert({text: 'success',type: 'success'});
             $('#myModal').modal('toggle');
           }, 1000);
         }else{
           myDropzone.processQueue();
         }
       });
       this.on("complete", function(data){
         console.log(data);
         if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
          {
           var _this = this;
           _this.removeAllFiles();
           $.smkProgressBar({
             element:'body',
             status:'start',
             bgColor: '#000',
             barColor: '#fff',
             content: 'Loading...'
           });
           setTimeout(function(){
             $.smkProgressBar({status:'end'});
             showTable();
             showSlidebar();
             $.smkAlert({text: 'success',type: 'success'});
             $('#myModal').modal('toggle');
           }, 1000);
          }
       });
      },
  });

</script>
