<?php
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$API_URL = 'https://api.line.me/v2/bot/message';
$ACCESS_TOKEN = getAccessToken();
$channelSecret = getChannelSecret();


$POST_HEADER = array('Content-Type: application/json', 'Authorization: Bearer ' . $ACCESS_TOKEN);


$request = file_get_contents('php://input');   // Get request content
$request_array = json_decode($request, true);   // Decode JSON to Array



if ( sizeof($request_array['events']) > 0 ) {

    foreach ($request_array['events'] as $event) {

        $reply_message = '';
        $reply_token = $event['replyToken'];
        $userId  = $event['source']['userId'];

        $lineMessage  = @$event['message']['text'];

        $sqls   = "SELECT * FROM t_member where user_id = '$userId'";
        $querys = DbQuery($sqls,null);
        $json   = json_decode($querys, true);
        $counts = $json['dataCount'];
        $rows   = $json['data'];

        //echo $lineMessage;

        if($lineMessage == "ลงทะเบียน" || $lineMessage == "")
        {
          $url = "https://sgdinterlinecctv.com/line/pages/register/index.php?userId=".$userId;

          $json = [
            "type" => "flex",
            "altText" => "ข้อความแจ้งเตือน",  // แก้ตรงนี้นะครับ
            "contents" => [
              "type"=> "carousel",
              "contents"=> [
                [
                  "type"=> "bubble",
                  "body"=> [
                    "type"=> "box",
                    "layout"=> "vertical",
                    "contents"=> [
                      [
                        "type"=> "text",
                        "text"=> "ลงทะเบียนแจ้งเตือนผ่าน Line",
                        "size"=> "md"
                      ]
                    ]
                  ],
                  "footer"=> [
                    "type"=> "box",
                    "layout"=> "horizontal",
                    "contents"=> [
                      [
                        "type"=> "button",
                        "action"=> [
                          "type"=> "uri",
                          "label"=> "ลงทะเบียน",
                          "uri"=> $url
                        ],
                        "style"=> "primary"
                      ]
                    ]
                  ]
                ]
              ]
            ]
          ];
        }else if ($counts > 0 && $lineMessage == "ลงทะเบียน"){
          $json = [
            "type"=> "text",
            'text' => "คุณ".$rows[0]['name']." ".$rows[0]['lname']." สมัครการแจ้งเตือนแล้ว"
          ];
        }else if ($lineMessage == "ปิดแจ้งเตือน"){
          $sql = "UPDATE t_member SET  is_active = 'N' WHERE 	user_id = '$userId'";
          $query   = DbQuery($sql,null);

          $json = [
            "type"=> "text",
            'text' => "ปิดการแจ้งเตือนสำเร็จ"
          ];
        }else if ($lineMessage == "เปิดแจ้งเตือน"){

          $sql = "UPDATE t_member SET  is_active = 'Y' WHERE 	user_id = '$userId'";
          $query      = DbQuery($sql,null);
          $json = [
            "type"=> "text",
            'text' => "เปิดการแจ้งเตือนสำเร็จ"
          ];
        }else if ($lineMessage != "ลงทะเบียน" && $lineMessage != ""){
          $json = [
            "type"=> "text",
            'text' => "ขอบคุณที่ส่งข้อความถึงเรา\n\nต้องขออภัยเป็นอย่างยิ่งที่บัญชีนี้ไม่สามารถตอบข้อความใดๆ ได้\nโปรดรอรับข่าวสารใหม่ๆ จากเราผ่านช่องทางนี้(content)"
          ];
        }

        // $data = [
        //     'replyToken' => $reply_token,
        //     'messages' => [['type' => 'text', 'text' => json_encode($request_array)]]
        // ];

        $data = [
            'replyToken' => $reply_token,
            'messages' => array($json)
        ];


        $post_body = json_encode($data, JSON_UNESCAPED_UNICODE);

        $send_result = send_reply_message($API_URL.'/reply', $POST_HEADER, $post_body);

        echo "Result: ".$send_result."\r\n";

    }
}

echo "OK";


function send_reply_message($url, $post_header, $post_body)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $post_header);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_body);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}

?>
