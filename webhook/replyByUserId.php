<?php
include('../inc/function/mainFunc.php');

// Access Token
$access_token = getAccessToken();
// User ID
$userId = 'Ub029d163fb65a4d0f51457d658c631c9';


// ข้อความที่ต้องการส่ง
$urlImg   = "https://scontent.fbkk7-2.fna.fbcdn.net/v/t1.0-9/35671730_1858799837474738_3555869489545347072_o.jpg?_nc_cat=106&_nc_sid=730e14&_nc_eui2=AeEPg1SXdg5TyAjF6ohn9ee6p2KwnSDt-qynYrCdIO36rDYUc6DruQKZrkfX5DOFlBZlWeoiaacuX4k-7jnCRoor&_nc_ohc=6DKGl3nYO_EAX_boZcY&_nc_ht=scontent.fbkk7-2.fna&oh=73dd28d9ea223fda6cb7dd7dd923100e&oe=5F89E1DA";
$dateTime = date("d-m-Y H:i:s");
$json = [
  "type" => "flex",
  "altText" => "ข้อความแจ้งเตือน",  // แก้ตรงนี้นะครับ
  "contents" => [
    "type" => "bubble",
    "header" => [
      "type" => "box",
      "layout" => "vertical",
      "contents" => [
        [
          "type" => "box",
          "layout" => "horizontal",
          "contents" => [
            [
              "type" => "image",
              "url" => $urlImg,
              "size" => "full",
              "aspectMode" => "cover",
              "aspectRatio" => "16:9",
              "gravity" => "center",
              "flex" => 1
            ],
            [
              "type" => "box",
              "layout" => "horizontal",
              "contents" => [
                [
                  "type" => "text",
                  "text" => "Alert",
                  "size" => "xs",
                  "color" => "#ffffff",
                  "align" => "center",
                  "gravity" => "center"
                ]
              ],
              "backgroundColor" => "#EC3D44",
              "paddingAll" => "2px",
              "paddingStart" => "4px",
              "paddingEnd" => "4px",
              "flex" => 0,
              "position" => "absolute",
              "offsetStart" => "18px",
              "offsetTop" => "18px",
              "cornerRadius" => "100px",
              "width" => "48px",
              "height" => "25px"
            ]
          ]
        ]
      ],
      "paddingAll" => "0px"
    ],
    "body" => [
      "type" => "box",
      "layout" => "vertical",
      "contents" => [
        [
          "type" => "box",
          "layout" => "vertical",
          "contents" => [
            [
              "type" => "box",
              "layout" => "vertical",
              "contents" => [
                [
                  "type" => "text",
                  "contents" => [],
                  "size" => "xl",
                  "wrap" => true,
                  "text" => "ระบบแจ้งเตือน",
                  "weight" => "bold"
                ],
                [
                  "type" => "text",
                  "text" => "กรุณาตรวจสอบกล้องวงจรปิด เนื่องจากมีผู้บุกรุกในพื้นที่",
                  "size" => "sm",
                  "wrap" => true
                ]
              ],
              "spacing" => "sm"
            ],
            [
              "type" => "box",
              "layout" => "vertical",
              "contents" => [
                [
                  "type" => "box",
                  "layout" => "baseline",
                  "contents" => [
                    [
                      "type" => "text",
                      "text" => "DateTime",
                      "flex" => 2,
                      "weight" => "bold",
                      "size" => "sm"
                    ],
                    [
                      "type" => "text",
                      "text" => $dateTime,
                      "align" => "start",
                      "flex" => 5,
                      "size" => "sm"
                    ]
                  ]
                ]
              ],
              "backgroundColor" => "#ffffff1A",
              "margin" => "xl"
            ]
          ]
        ]
      ],
      "paddingAll" => "20px"
    ],
    "footer" => [
      "type" => "box",
      "layout" => "vertical",
      "contents" => [
        [
          "type" => "text",
          "text" => "CCTV FUJIKO & KENPRO",
          "size" => "md",
          "weight" => "bold",
          "align" => "center",
          "color" => "#ffffff"
        ]
      ]
    ],
    "styles" => [
      "footer" => [
        "backgroundColor" => "#1b519b"
      ]
    ]
  ]
];

//print_r($json);

$post = json_encode(array(
    'to' => array($userId),
    'messages' => array($json),
));
//URL ของบริการ Replies สำหรับการตอบกลับด้วยข้อความอัตโนมัติ
$url = 'https://api.line.me/v2/bot/message/multicast';
$headers = array('Content-Type: application/json', 'Authorization: Bearer '.$access_token);
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
$result = curl_exec($ch);
echo $result;
?>
