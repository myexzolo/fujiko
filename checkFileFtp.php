<?php
include('inc/function/connect.php');
include('inc/function/mainFunc.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$sql   = "SELECT n.* , f.user  FROM t_code_nvr n, t_ftp f WHERE n.is_active = 'Y' and n.id_ftp = f.id ";
$querys     = DbQuery($sql,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];

if($dataCount > 0)
{
  foreach ($rows as $key => $value)
  {
    $id   = $value['id'];
    $code = $value['code'];
    $user = $value['user'];
    $last_file_date = @$value['last_file_date'];

    $rootpath = "../ftp_access_motion/$user";
    //$rootpath = "cctv/$code";

    if (file_exists("$rootpath"))
    {
      $files = glob($rootpath."/*"); // get all file names
      //print_r($files);
      foreach($files as $file)
      { // iterate files

        if(is_file($file))
        {
            if(is_image($file))
            {
              $interval   = 1;
              $dateModify  = date("Y-m-d H:i:s", filemtime($file));
              $dateModShow = date("d-m-Y H:i:s", filemtime($file));
              $fileInfo   = pathinfo($file);

              $path = $rootpath."/";
              $nameImg = resizeImage($file,'800','',$path,'');

              // $urlImg     = "https://sgdinterlinecctv.com/ftp_access_motion/$code/".$nameImg;
              $urlImg     = $rootpath."/".$nameImg;

              if($last_file_date != "")
              {
                //$last_file_date = date("Y-m-d H:i:s", $last_file_date);
                echo "last_file_date :".$last_file_date.", dateModify :".$dateModify."\r\n";
                $interval       = DateTimeDiff($last_file_date, $dateModify);
                echo " ==> interval :".$interval."\r\n";
              }

              if($interval > 0)
              {
                $sqln   = "UPDATE t_code_nvr SET last_file_date = NOW() WHERE id = $id ";
                $querys  = DbQuery($sqln,null);


                $sqlm   = "SELECT * FROM t_member WHERE code = '$code' and is_active = 'Y' ";

                $querym      = DbQuery($sqlm,null);
                $jsonm       = json_decode($querym, true);
                $dataCountm  = $jsonm['dataCount'];
                $rowm        = $jsonm['data'];

                for($x=0;$x < $dataCountm; $x++)
                {
                  $token =  $rowm[$x]['line_token'];
                  if($token != ""){
                    echo "token :".$token." ,code :".$code.",urlImg :".$urlImg." ,dateModShow :".$dateModShow."\r\n";
                    sendLineMessages($token,$urlImg,$dateModShow);
                    sleep(1);
                  }
                }
                unlink($file);
              }else{
                //unlink($file);
              }
            }else{
              unlink($file);
            }
        }
      }
    }
  }
}


$sql   = "SELECT n.* , f.user  FROM t_code_nvr n, t_ftp f WHERE n.is_active <> 'Y' and n.id_ftp = f.id ";
$querys     = DbQuery($sql,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];

if($dataCount > 0)
{
  foreach ($rows as $key => $value)
  {
    $id   = $value['id'];
    $code = $value['code'];
    $user = $value['user'];
    $last_file_date = @$value['last_file_date'];

    $rootpath = "../ftp_access_motion/$user";
    //$rootpath = "cctv/$code";

    if (file_exists("$rootpath"))
    {
      $files = glob($rootpath."/*"); // get all file names
      //print_r($files);
      foreach($files as $file)
      { // iterate files

        if(is_file($file))
        {
            if(is_image($file))
            {
              unlink($file);
            }
        }
      }
    }
  }
}


function is_image($path)
{
    $a = getimagesize($path);
    $image_type = $a[2];

    if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
    {
        return true;
    }
    return false;
}


function sendLineMessages($token,$urlImg,$dateTime)
{
  if (function_exists('curl_file_create')) {
  $cFile = curl_file_create($urlImg );
  } else {
  $cFile = '@'.realpath($urlImg );
  }

  // $fields = [
  //               'message' => 'ระบบแจ้งเตือน\r\nกรุณาตรวจสอบกล้องวงจรปิด เนื่องจากมีผู้บุกรุกในพื้นที \r\n\r\n DateTime '.$dateTime.'\r\n\r\n CCTV FUJIKO & KENPRO',
  //               'imageThumbnail' => $urlImg,
  //               'imageFullsize' => $urlImg,
  //           ];
  $dateTime  = explode(" ", $dateTime);
  $message  = "";
  $message  .= "\r\nระบบแจ้งเตือน";
  $message  .= "\r\n--------------------------";
  $message  .= "\r\nกรุณาตรวจสอบกล้องวงจรปิด เนื่องจากมีผู้บุกรุกในพื้นที่";
  $message  .= "\r\n\r\nDate ".$dateTime[0];
  $message  .= "\r\nTime ".$dateTime[1];
  $message  .= "\r\n--------------------------";
  $message  .= "\r\nCCTV FUJIKO & KENPRO";

  $fields = [
                'message' => $message,
                'imageFile' => $cFile,
            ];

  $headers = [
                'Authorization: Bearer ' . $token
            ];
  //URL ของบริการ Replies สำหรับการตอบกลับด้วยข้อความอัตโนมัติ
  // $url = 'https://api.line.me/v2/bot/message/multicast';
  // $url = 'https://api.line.me/v2/bot/message/push';
  $url = 'https://notify-api.line.me/api/notify';
  //$headers = array('Content-Type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$token);

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_POST, count($fields));
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


  $result = curl_exec($ch);
  curl_close($ch);

  echo "result : ".$result."\r\n";
  if($result != "")
  {
    $arrRes = json_decode($result, true);
    print_r($arrRes);
    if(isset($arrRes['status']))
    {
      if($arrRes['status'] == "400")
      {
        sendLineErrorMessages($token,$message,$arrRes['message']);
      }
    }
  }
}


function sendLineErrorMessages($token,$message,$errmess)
{
  $message  .= "\r\n--------------------------";
  $message  .= "\r\nError Message : ".$errmess;
  $fields = [
                'message' => $message
            ];

  $headers = [
                'Authorization: Bearer ' . $token
            ];
  //URL ของบริการ Replies สำหรับการตอบกลับด้วยข้อความอัตโนมัติ
  // $url = 'https://api.line.me/v2/bot/message/multicast';
  // $url = 'https://api.line.me/v2/bot/message/push';
  $url = 'https://notify-api.line.me/api/notify';
  //$headers = array('Content-Type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$token);

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_POST, count($fields));
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


  $result = curl_exec($ch);
  curl_close($ch);
  $arrRes = json_decode($result);
  echo $result."\r\n";
}
?>
