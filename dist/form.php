<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action       = @$_POST['action'];
$id           = @$_POST['id'];
$code         = "";
$detail       = "";
$is_active    = "";
$readonly     = "";


if($action == 'EDIT'){
  $btn = 'Update changes';
  $readonly = "readonly";

  $sql   = "SELECT * FROM t_code_nvr WHERE id = '$id' and is_active <> 'D' ";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  $code         = $row[0]['code'];
  $name         = $row[0]['name'];
  $detail       = $row[0]['detail'];
  $is_active    = $row[0]['is_active'];

}
if($action == 'ADD'){
 $btn = 'Save changes';
}
?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="id" value="<?=@$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>ซีเรียลนัมเบอร์</label>
        <input value="<?=@$code?>" name="code" type="text" class="form-control" placeholder="Code" required <?=$readonly ?>>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>ชื่อเครื่อง</label>
        <input value="<?=@$name?>" name="name" type="text" class="form-control" placeholder="Name" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Status</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>รายละเอียด</label>
        <input value="<?=@$detail?>" name="detail" type="text" class="form-control" placeholder="" >
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
